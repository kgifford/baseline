FROM node:carbon

#Create app directory
WORKDIR /usr/src/app

#Insall app dependencies
# A wildcard is used to ensure both package.json ADN package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm insall --only=production

#Bundle app source
COPY . .

EXPOSE 8080
CMD ["npm", "start" ]
